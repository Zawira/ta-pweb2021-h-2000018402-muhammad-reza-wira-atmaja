<!DOCTYPE html>

<head>
    <tittle><center><h1>Belajar <span>php</span></h1></center></tittle>
    <link rel="stylesheet" href="php.css">
</head>
<hr>
<body>
    <div class="main">
        <div class="satu">
        <p> Listing program 9.1 : </p>
        <?php 

            $gaji = 1000000;
            $pajak = 0.1;
            $thp = $gaji - ($gaji * $pajak);

            echo "Gaji sebelum pajak : Rp. $gaji <br>";

            echo "Gaji yang dibawa pulang : Rp. $thp ";
        ?>
        </div>

        <div class="dua">
        <p> Listing program 9.2 : </p>
        <?php
            $a = 5;
            $b = 4;

            echo "$a==$b : ". ($a == $b);
            echo "<br> $a != $b : ". ($a != $b);
            echo "<br> $a > $b : ". ($a > $b);
            echo "<br> $a < $b : ". ($a < $b);
            echo "<br> ($a == $b) && ($a > $b) : ". (($a != $b) && ($a > $b));
            echo "<br> ($a == $b) || ($a > $b) : ". (($a != $b) || ($a > $b));
        ?>
        </div>
    </div>
</body>